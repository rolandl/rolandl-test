package fr.rolandl.wrapper;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.UiThread;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.smartnsoft.droid4me.app.AppPublics;

import com.smartnsoft.recyclerview.attributes.SmartRecyclerAttributes;
import com.smartnsoft.recyclerview.wrapper.DiffUtilSmartRecyclerViewWrapper;
import fr.rolandl.fragment.MainFragment;
import fr.rolandl.room.bo.Address;

/**
 * @author Ludovic Roland
 * @since 2017.10.21
 */
public final class AddressWrapper
    extends DiffUtilSmartRecyclerViewWrapper<Address>
{

  public static final class AddressAttributes
      extends SmartRecyclerAttributes<Address>
  {

    private final TextView textView;

    public AddressAttributes(View view)
    {
      super(view);
      textView = view.findViewById(android.R.id.text1);
    }

    @Override
    public void update(final Activity activity, final Address businessObject, boolean isSelected)
    {
      update(businessObject);

      itemView.setOnClickListener(new OnClickListener()
      {
        @Override
        public void onClick(View v)
        {
          final Intent intent = new Intent(MainFragment.ADDRESS_SELECTED_ACTION);
          intent.putExtra(AppPublics.EXTRA_BUSINESS_OBJECT, businessObject);
          LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
        }
      });
    }

    @UiThread
    public void update(Address businessObject)
    {
      textView.setText(businessObject.label);
    }

  }

  public AddressWrapper(Address businessObject)
  {
    super(businessObject, 0, android.R.layout.simple_list_item_1);
  }

  @Override
  public long getDiffUtilId()
  {
    return getBusinessObject().id;
  }

  @Override
  public long getDiffUtilHashCode()
  {
    return getBusinessObject().hashCode();
  }

  @Override
  protected Object extractNewViewAttributes(Activity activity, View view, Address address)
  {
    return new AddressAttributes(view);
  }

}
