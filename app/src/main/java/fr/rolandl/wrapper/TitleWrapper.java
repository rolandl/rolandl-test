package fr.rolandl.wrapper;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.annotation.UiThread;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;

import com.smartnsoft.recyclerview.attributes.SmartRecyclerAttributes;
import com.smartnsoft.recyclerview.wrapper.DiffUtilSmartRecyclerViewWrapper;
import fr.rolandl.R;

/**
 * @author Ludovic Roland
 * @since 2017.10.22
 */
public final class TitleWrapper
    extends DiffUtilSmartRecyclerViewWrapper<Void>
{

  public static final class TitleAttributes
      extends SmartRecyclerAttributes<Void>
  {

    private final TextView textView;

    public TitleAttributes(View view)
    {
      super(view);
      textView = view.findViewById(android.R.id.text1);
    }

    @Override
    public void update(final Activity activity, final Void businessObject, boolean isSelected)
    {
      update();
    }

    @UiThread
    public void update()
    {
      final String string = textView.getResources().getString(R.string.historical);
      final SpannableString spannableString = new SpannableString(string);
      final StyleSpan styleSpan = new StyleSpan(Typeface.BOLD_ITALIC);
      spannableString.setSpan(styleSpan, 0, string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

      textView.setText(spannableString);
    }

  }

  public TitleWrapper()
  {
    super(null, 1, android.R.layout.simple_list_item_1);
  }

  @Override
  public long getDiffUtilId()
  {
    return 0;
  }

  @Override
  public long getDiffUtilHashCode()
  {
    return 0;
  }

  @Override
  protected Object extractNewViewAttributes(Activity activity, View view, Void v)
  {
    return new TitleAttributes(view);
  }

}
