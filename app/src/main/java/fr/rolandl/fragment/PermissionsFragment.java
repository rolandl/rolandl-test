package fr.rolandl.fragment;

import android.Manifest.permission;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import fr.rolandl.MainActivity;
import fr.rolandl.R;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

/**
 * @author Ludovic Roland
 * @since 2017.10.18
 */
@RuntimePermissions
public final class PermissionsFragment
    extends Fragment
    implements OnClickListener
{

  private Button askForPermissions;

  private TextView description;

  private boolean shouldDisplaySettings;

  private LocationManager locationManager;

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
  {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    PermissionsFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
  {
    final View rootView = inflater.inflate(R.layout.fragment_permissions, container, false);

    description = rootView.findViewById(android.R.id.text1);
    askForPermissions = rootView.findViewById(R.id.askForPermissions);

    return rootView;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
  {
    askForPermissions.setOnClickListener(this);

    if (ActivityCompat.checkSelfPermission(getContext(), permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
    {
      description.setText(R.string.permissions_rational);
      PermissionsFragmentPermissionsDispatcher.askForPermissionsWithPermissionCheck(this);
    }
    else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) == false)
    {
      //note : if we really really want to do something proper, we should use the SettingsClient class
      //see : https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
      description.setText(R.string.gps_rational);
      askForPermissions.setText(android.R.string.ok);
    }
  }

  @Override
  public void onClick(View view)
  {
    if (ActivityCompat.checkSelfPermission(getContext(), permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
    {
      if (shouldDisplaySettings == false)
      {
        PermissionsFragmentPermissionsDispatcher.askForPermissionsWithPermissionCheck(this);
      }
      else
      {
        if (getActivity() != null && getActivity().isFinishing() == false)
        {
          final Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
          intent.setData(Uri.fromParts("package", getActivity().getPackageName(), null));
          startActivityForResult(intent, MainActivity.REQUEST_PERMISSION_CODE);
        }
      }
    }
    else
    {
      LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(MainActivity.CHECK_PERMISSIONS_AND_GPS_PROVIDER_ACTION));
    }
  }

  @NeedsPermission(permission.ACCESS_FINE_LOCATION)
  void askForPermissions()
  {
    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(MainActivity.CHECK_PERMISSIONS_AND_GPS_PROVIDER_ACTION));
  }

  @OnPermissionDenied(permission.ACCESS_FINE_LOCATION)
  void askForPermissionsDenied()
  {
    description.setText(R.string.permissions_rational);
    shouldDisplaySettings = false;
  }

  @OnNeverAskAgain(permission.ACCESS_FINE_LOCATION)
  void askForPermissionsRefused()
  {
    description.setText(R.string.permissions_settings);
    shouldDisplaySettings = true;
  }

}
