package fr.rolandl.fragment;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.smartnsoft.droid4me.app.AppPublics;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.jakewharton.rxbinding2.widget.RxTextView;
import fr.rolandl.BuildConfig;
import fr.rolandl.R;
import fr.rolandl.adapter.PlacesAdapter;
import fr.rolandl.map.MapEventListener;
import fr.rolandl.map.MapWrapper;
import fr.rolandl.room.db.AddressDatabase;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * @author Ludovic Roland
 * @since 2017.10.18
 */
public final class MainFragment
    extends Fragment
    implements OnConnectionFailedListener, ConnectionCallbacks, LocationListener, OnClickListener, OnItemClickListener, MapEventListener
{

  @IntDef({ MainFragment.ADDRESS_MODE, MainFragment.USER_MODE, MainFragment.CENTER_MODE })
  @Retention(RetentionPolicy.SOURCE)
  private @interface DisplayMode
  {

  }

  public static final String ADDRESS_SELECTED_ACTION = "addressSelectedAction";

  private static final int ADDRESS_MODE = 0;

  private static final int USER_MODE = MainFragment.ADDRESS_MODE + 1;

  private static final int CENTER_MODE = MainFragment.USER_MODE + 1;

  private static final long LOCATION_REQUEST_INTERVAL_IN_MS = 1 * 1000;

  private static final long GEO_API_TIMEOUT_IN_MS = 5 * 1000;

  private static final long SEARCH_DEBOUNCE_TIME_IN_MS = 300;

  @DisplayMode
  private int displayMode = MainFragment.USER_MODE;

  private GoogleApiClient googleApiClient;

  private LocationRequest locationRequest;

  private Location latestUserLocation;

  private MapWrapper mapWrapper;

  private FloatingActionButton gps;

  private FloatingActionButton center;

  private ImageButton clear;

  private ImageView centeredMarker;

  private ProgressBar progressBar;

  private AutoCompleteTextView search;

  private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
  {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      if (intent.getAction().equals(MainFragment.ADDRESS_SELECTED_ACTION) == true)
      {
        final fr.rolandl.room.bo.Address address = (fr.rolandl.room.bo.Address) intent.getSerializableExtra(AppPublics.EXTRA_BUSINESS_OBJECT);
        Timber.d("Address selected : '" + address.label + "'");

        updateDisplayMode(MainFragment.ADDRESS_MODE);
        search.setText(address.label);
        mapWrapper.centerMapOnTarget(address.latitude, address.longitude);

        address.timestamp = System.currentTimeMillis();
        Observable.just(address)
            .flatMap(new Function<fr.rolandl.room.bo.Address, ObservableSource<fr.rolandl.room.bo.Address>>()
            {
              @Override
              public ObservableSource<fr.rolandl.room.bo.Address> apply(fr.rolandl.room.bo.Address selectedAddress)
                  throws Exception
              {
                AddressDatabase.getInstance().insertAddress(selectedAddress);
                return Observable.just(selectedAddress);
              }
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe();
      }
    }
  };

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    final GoogleApiClient.Builder googleApiClientbuilder = new GoogleApiClient.Builder(getContext());
    googleApiClientbuilder.addOnConnectionFailedListener(this);
    googleApiClientbuilder.addConnectionCallbacks(this);
    googleApiClientbuilder.addApi(LocationServices.API);
    googleApiClientbuilder.addApi(Places.GEO_DATA_API);

    googleApiClient = googleApiClientbuilder.build();

    locationRequest = LocationRequest.create();
    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    locationRequest.setInterval(MainFragment.LOCATION_REQUEST_INTERVAL_IN_MS);

    mapWrapper = new MapWrapper(this, BuildConfig.APPLICATION_ID.contains("mapbox") == true ? MapWrapper.MAPBOX : MapWrapper.GOOGLE_MAP);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
  {
    final View rootView = inflater.inflate(R.layout.fragment_main, container, false);

    gps = rootView.findViewById(R.id.gps);
    center = rootView.findViewById(R.id.center);
    clear = rootView.findViewById(R.id.clear);
    search = rootView.findViewById(R.id.search);
    progressBar = rootView.findViewById(R.id.progressBar);
    centeredMarker = rootView.findViewById(R.id.centeredMarker);

    mapWrapper.onCreateView(getChildFragmentManager(), R.id.mapPlaceholder);

    return rootView;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);

    mapWrapper.getMapAsync();

    hideOrDisplayProgressBar(false);

    gps.setOnClickListener(this);
    center.setOnClickListener(this);
    clear.setOnClickListener(this);

    search.setOnItemClickListener(this);

    updateDisplayMode(MainFragment.USER_MODE);

    RxTextView.textChanges(search)
        .debounce(MainFragment.SEARCH_DEBOUNCE_TIME_IN_MS, TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .filter(new Predicate<CharSequence>()
        {
          @Override
          public boolean test(CharSequence charSequence)
              throws Exception
          {
            final boolean accept = charSequence.length() > 2;

            if (accept == true)
            {
              hideOrDisplayProgressBar(true);
            }

            return accept;
          }
        })
        .observeOn(Schedulers.io())
        .switchMap(new Function<CharSequence, ObservableSource<ArrayList<AutocompletePrediction>>>()
        {
          @Override
          public ObservableSource<ArrayList<AutocompletePrediction>> apply(CharSequence charSequence)
              throws Exception
          {
            final PendingResult<AutocompletePredictionBuffer> autocompletePredictions = Places.GeoDataApi.getAutocompletePredictions(googleApiClient, charSequence.toString(), null, null);
            final AutocompletePredictionBuffer autocompletePredictionBuffer = autocompletePredictions.await(MainFragment.GEO_API_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS);

            if (autocompletePredictionBuffer.getStatus().isSuccess() == false)
            {
              autocompletePredictionBuffer.release();
              return Observable.empty();
            }

            return Observable.just(DataBufferUtils.freezeAndClose(autocompletePredictionBuffer));
          }
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<ArrayList<AutocompletePrediction>>()
        {
          @Override
          public void accept(ArrayList<AutocompletePrediction> autocompletePredictions)
              throws Exception
          {
            hideOrDisplayProgressBar(false);
            search.setAdapter(new PlacesAdapter(getContext(), autocompletePredictions));
          }
        }, new Consumer<Throwable>()
        {
          @Override
          public void accept(Throwable throwable)
              throws Exception
          {
            displaySnackbar();
            hideOrDisplayProgressBar(false);
            Timber.w(throwable);
          }
        });
  }

  @Override
  public void onResume()
  {
    super.onResume();
    googleApiClient.connect();
    LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver, getIntentFilter());
  }

  @Override
  public void onPause()
  {
    if (googleApiClient != null && googleApiClient.isConnected() == true)
    {
      googleApiClient.disconnect();
    }

    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);

    super.onPause();
  }

  @Override
  @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
  @SuppressWarnings("MissingPermission")
  public void onConnected(@Nullable Bundle bundle)
  {
    //note : Event if the FusedLocationProviderApi class is deprecated, Google asks developers to continue to use it until Google Play services version 12.0.0 is available
    //see : https://developer.android.com/training/location/receive-location-updates.html
    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
  }

  @Override
  public void onConnectionSuspended(int i)
  {
    displaySnackbar();
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
  {
    displaySnackbar();
  }

  @Override
  public void onLocationChanged(Location location)
  {
    Timber.d("Latitude : '" + location.getLatitude() + "' | Longitude : '" + location.getLongitude() + "'");

    latestUserLocation = location;

    if (displayMode == MainFragment.USER_MODE)
    {
      mapWrapper.centerMapOnTarget(latestUserLocation.getLatitude(), latestUserLocation.getLongitude());
    }
  }

  @Override
  public void onClick(View view)
  {
    if (view == gps)
    {
      updateDisplayMode(MainFragment.USER_MODE);
    }
    else if (view == center)
    {
      updateDisplayMode(MainFragment.CENTER_MODE);
    }
    else if (view == clear)
    {
      clearTextView();
    }
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id)
  {
    updateDisplayMode(MainFragment.ADDRESS_MODE);
    final AutocompletePrediction selectedAddress = (AutocompletePrediction) parent.getItemAtPosition(position);

    hideOrDisplayProgressBar(true);
    Observable.just(selectedAddress)
        .switchMap(new Function<AutocompletePrediction, ObservableSource<Place>>()
        {
          @Override
          public ObservableSource<Place> apply(AutocompletePrediction selectedAddress)
              throws Exception
          {
            final PendingResult<PlaceBuffer> placeById = Places.GeoDataApi.getPlaceById(googleApiClient, selectedAddress.getPlaceId());
            final PlaceBuffer placeBuffer = placeById.await(MainFragment.GEO_API_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS);

            if (placeBuffer.getStatus().isSuccess() == false)
            {
              placeBuffer.release();
              return Observable.empty();
            }

            final Place place = DataBufferUtils.freezeAndClose(placeBuffer).get(0);
            AddressDatabase.getInstance().insertAddress(new fr.rolandl.room.bo.Address(selectedAddress.getFullText(null).toString(), place.getLatLng().latitude, place.getLatLng().longitude));

            return Observable.just(place);
          }
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<Place>()
        {
          @Override
          public void accept(Place place)
              throws Exception
          {
            hideOrDisplayProgressBar(false);
            mapWrapper.centerMapOnTarget(place.getLatLng().latitude, place.getLatLng().longitude);
          }
        }, new Consumer<Throwable>()
        {
          @Override
          public void accept(Throwable throwable)
              throws Exception
          {
            displaySnackbar();
            hideOrDisplayProgressBar(false);
            Timber.w(throwable);
          }
        });
  }

  @Override
  public void onCameraMove()
  {
    if (displayMode == MainFragment.CENTER_MODE)
    {
      hideOrDisplayProgressBar(true);
    }
  }

  @Override
  public void onCameraIdle()
  {
    if (displayMode == MainFragment.CENTER_MODE)
    {
      retrieveAndDisplayCenteredAddress();
    }
  }

  @Override
  public void onMapReady()
  {
    if (latestUserLocation != null)
    {
      mapWrapper.centerMapOnTarget(latestUserLocation.getLatitude(), latestUserLocation.getLongitude());
    }
  }

  private void displaySnackbar()
  {
    Snackbar.make(getView(), R.string.issue, Snackbar.LENGTH_SHORT).show();
  }

  @NonNull
  private IntentFilter getIntentFilter()
  {
    final IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(MainFragment.ADDRESS_SELECTED_ACTION);

    return intentFilter;
  }

  @UiThread
  private void retrieveAndDisplayCenteredAddress()
  {
    hideOrDisplayProgressBar(true);
    final Location location = mapWrapper.getCameraPositionTarget();

    Observable.just(location)
        .switchMap(new Function<Location, ObservableSource<fr.rolandl.room.bo.Address>>()
        {
          @Override
          public ObservableSource<fr.rolandl.room.bo.Address> apply(Location location)
              throws Exception
          {
            //note : normally we have to check if the geocoder is present or not
            //see : https://developer.android.com/reference/android/location/Geocoder.html#isPresent()
            final Geocoder geocoder = new Geocoder(getContext());
            final List<Address> results = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            if (results == null || results.isEmpty() == true)
            {
              return Observable.empty();
            }

            final Address address = results.get(0);
            final StringBuilder addressBuilder = new StringBuilder();
            if (TextUtils.isEmpty(address.getSubThoroughfare()) == false)
            {
              addressBuilder.append(address.getSubThoroughfare()).append(" ");
            }
            if (TextUtils.isEmpty(address.getThoroughfare()) == false)
            {
              addressBuilder.append(address.getThoroughfare()).append(" ");
            }
            if (TextUtils.isEmpty(address.getPostalCode()) == false)
            {
              addressBuilder.append(address.getPostalCode()).append(" ");
            }
            if (TextUtils.isEmpty(address.getLocality()) == false)
            {
              addressBuilder.append(address.getLocality());
            }

            final fr.rolandl.room.bo.Address newAddress = new fr.rolandl.room.bo.Address(addressBuilder.toString(), location.getLatitude(), location.getLongitude());
            AddressDatabase.getInstance().insertAddress(newAddress);

            return Observable.just(newAddress);
          }
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<fr.rolandl.room.bo.Address>()
        {
          @Override
          public void accept(fr.rolandl.room.bo.Address address)
              throws Exception
          {
            search.setText(address.label);
            hideOrDisplayProgressBar(false);
          }
        }, new Consumer<Throwable>()
        {
          @Override
          public void accept(Throwable throwable)
              throws Exception
          {
            displaySnackbar();
            hideOrDisplayProgressBar(false);
            Timber.w(throwable);
          }
        });
  }

  @UiThread
  private void updateDisplayMode(@DisplayMode int displayMode)
  {
    this.displayMode = displayMode;

    switch (this.displayMode)
    {
      case MainFragment.ADDRESS_MODE:
        hideOrDisplayCenteredMarker(false);
        break;

      case MainFragment.USER_MODE:
        clearTextView();
        hideOrDisplayCenteredMarker(false);

        if (latestUserLocation != null)
        {
          mapWrapper.centerMapOnTarget(latestUserLocation.getLatitude(), latestUserLocation.getLongitude());
        }
        break;

      case MainFragment.CENTER_MODE:
        clearTextView();

        mapWrapper.removeMarker();

        hideOrDisplayCenteredMarker(true);
        retrieveAndDisplayCenteredAddress();
        break;
    }
  }

  @UiThread
  private void hideOrDisplayCenteredMarker(boolean display)
  {
    centeredMarker.setVisibility(display == true ? View.VISIBLE : View.INVISIBLE);
  }

  @UiThread
  private void hideOrDisplayProgressBar(boolean display)
  {
    progressBar.setVisibility(display == true ? View.VISIBLE : View.INVISIBLE);
  }

  @UiThread
  private void clearTextView()
  {
    search.setText("");
  }

}
