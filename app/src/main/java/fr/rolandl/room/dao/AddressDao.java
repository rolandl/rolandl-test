package fr.rolandl.room.dao;

import java.util.List;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import fr.rolandl.room.bo.Address;

/**
 * @author Ludovic Roland
 * @since 2017.10.21
 */
@Dao
public interface AddressDao
{

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insert(Address address);

  @Query("SELECT * FROM Address ORDER BY timestamp DESC LIMIT 15")
  LiveData<List<Address>> getAll();

  @Query("DELETE FROM Address WHERE id NOT IN (SELECT id FROM Address ORDER BY timestamp DESC LIMIT 15)")
  void cleanUp();

}
