package fr.rolandl.room.db;

import java.util.List;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.RestrictTo;
import android.support.annotation.RestrictTo.Scope;
import android.support.annotation.WorkerThread;

import fr.rolandl.room.bo.Address;
import fr.rolandl.room.dao.AddressDao;

/**
 * @author Ludovic Roland
 * @since 2017.10.21
 */
@Database(entities = { Address.class }, version = 1, exportSchema = false)
public abstract class AddressDatabase
    extends RoomDatabase
{

  private static volatile AddressDatabase instance;

  public static void init(Context context, boolean isTestMode)
  {
    if (instance == null)
    {
      synchronized (AddressDatabase.class)
      {
        if (instance == null)
        {
          if(isTestMode == false)
          {
            instance = Room.databaseBuilder(context, AddressDatabase.class, "database").fallbackToDestructiveMigration().build();
          }
          else
          {
            instance = Room.inMemoryDatabaseBuilder(context, AddressDatabase.class).build();
          }
        }
      }
    }
    else
    {
      throw new IllegalStateException("The Database instance is already initialized");
    }
  }

  public static AddressDatabase getInstance()
  {
    if (instance == null)
    {
      throw new IllegalStateException("Please call init before call getInstance");
    }

    return instance;
  }

  @WorkerThread
  public void insertAddress(Address address)
  {
    addressDao().insert(address);
    addressDao().cleanUp();
  }

  @WorkerThread
  public LiveData<List<Address>> getAll()
  {
    return addressDao().getAll();
  }

  @RestrictTo(Scope.SUBCLASSES)
  public abstract AddressDao addressDao();

}
