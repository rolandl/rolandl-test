package fr.rolandl.room.bo;

import java.io.Serializable;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * @author Ludovic Roland
 * @since 2017.10.21
 */
@Entity
public final class Address
    implements Serializable
{

  @NonNull
  public final String label;

  public final double latitude;

  public final double longitude;

  @PrimaryKey
  public int id;

  public long timestamp;

  public Address(@NonNull String label, double latitude, double longitude)
  {
    this.label = label;
    this.latitude = latitude;
    this.longitude = longitude;
    this.id = generateId(latitude, longitude);
    this.timestamp = System.currentTimeMillis();
  }

  @Override
  public int hashCode()
  {
    int result;
    long temp;
    result = label.hashCode();
    temp = Double.doubleToLongBits(latitude);
    result = 31 * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(longitude);
    result = 31 * result + (int) (temp ^ (temp >>> 32));
    result = 31 * result + id;
    result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
    return result;
  }

  private int generateId(double latitude, double longitude)
  {
    int result;
    long temp;
    temp = Double.doubleToLongBits(latitude);
    result = (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(longitude);
    return 31 * result + (int) (temp ^ (temp >>> 32));
  }

}
