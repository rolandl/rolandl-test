package fr.rolandl.viewmodel;

import java.util.List;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import fr.rolandl.room.bo.Address;
import fr.rolandl.room.db.AddressDatabase;

/**
 * @author Ludovic Roland
 * @since 2017.10.21
 */
public final class AddressViewModel
    extends AndroidViewModel
{

  public final LiveData<List<Address>> addresses;

  public AddressViewModel(@NonNull Application application)
  {
    super(application);
    addresses = AddressDatabase.getInstance().getAll();
  }

}
