package fr.rolandl.map;

/**
 * @author Ludovic Roland
 * @since 2017.10.21
 */
public interface MapEventListener
{

  void onMapReady();

  void onCameraIdle();

  void onCameraMove();

}
