package fr.rolandl.map;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import android.location.Location;
import android.support.annotation.IdRes;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.MapboxMap.OnCameraIdleListener;
import com.mapbox.mapboxsdk.maps.MapboxMap.OnCameraMoveListener;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import fr.rolandl.R;

/**
 * @author Ludovic Roland
 * @since 2017.10.21
 */
public final class MapWrapper
    implements Serializable, OnMapReadyCallback, com.google.android.gms.maps.OnMapReadyCallback, OnCameraIdleListener, GoogleMap.OnCameraIdleListener, OnCameraMoveListener, GoogleMap.OnCameraMoveListener
{

  @IntDef({ MapWrapper.MAPBOX, MapWrapper.GOOGLE_MAP })
  @Retention(RetentionPolicy.SOURCE)
  private @interface MapMode
  {

  }

  public static final int MAPBOX = 0;

  public static final int GOOGLE_MAP = MapWrapper.MAPBOX + 1;

  private static final int DEFAULT_MAP_ZOOM_LEVEL = 16;

  @MapMode
  private final int mapMode;

  @NonNull
  private MapEventListener mapEventListener;

  private com.mapbox.mapboxsdk.maps.SupportMapFragment mapboxFragment;

  private MapboxMap mapboxMap;

  private com.mapbox.mapboxsdk.annotations.Marker mapboxMarker;

  private com.google.android.gms.maps.SupportMapFragment googleMapFragment;

  private GoogleMap googleMap;

  private com.google.android.gms.maps.model.Marker googleMarker;

  public MapWrapper(@NonNull MapEventListener mapEventListener, @MapMode int mapMode)
  {
    this.mapEventListener = mapEventListener;
    this.mapMode = mapMode;
  }

  @Override
  public void onMapReady(MapboxMap mapboxMap)
  {
    this.mapboxMap = mapboxMap;
    this.mapboxMap.setOnCameraIdleListener(this);
    this.mapboxMap.setOnCameraMoveListener(this);

    mapEventListener.onMapReady();
  }

  @Override
  public void onMapReady(GoogleMap googleMap)
  {
    this.googleMap = googleMap;
    this.googleMap.setOnCameraIdleListener(this);
    this.googleMap.setOnCameraMoveListener(this);

    mapEventListener.onMapReady();
  }

  @Override
  public void onCameraIdle()
  {
    mapEventListener.onCameraIdle();
  }

  @Override
  public void onCameraMove()
  {
    mapEventListener.onCameraMove();
  }

  @UiThread
  public void onCreateView(FragmentManager fragmentManager, @IdRes int placeholderId)
  {
    final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

    if (mapMode == MapWrapper.MAPBOX)
    {
      mapboxFragment = com.mapbox.mapboxsdk.maps.SupportMapFragment.newInstance();
    }
    else if (mapMode == MapWrapper.GOOGLE_MAP)
    {
      googleMapFragment = com.google.android.gms.maps.SupportMapFragment.newInstance();
    }

    fragmentTransaction.replace(placeholderId, mapboxFragment != null ? mapboxFragment : googleMapFragment);
    fragmentTransaction.commitAllowingStateLoss();
  }

  @UiThread
  public void getMapAsync()
  {
    if (mapMode == MapWrapper.MAPBOX)
    {
      mapboxFragment.getMapAsync(this);
    }
    else if (mapMode == MapWrapper.GOOGLE_MAP)
    {
      googleMapFragment.getMapAsync(this);
    }
  }

  @UiThread
  public void centerMapOnTarget(double latitude, double longitude)
  {
    if (mapMode == MapWrapper.MAPBOX && mapboxMap != null)
    {
      final com.mapbox.mapboxsdk.geometry.LatLng targetPosition = new com.mapbox.mapboxsdk.geometry.LatLng(latitude, longitude);
      final com.mapbox.mapboxsdk.camera.CameraPosition.Builder cameraPositionBuilder = new com.mapbox.mapboxsdk.camera.CameraPosition.Builder(mapboxMap.getCameraPosition());
      cameraPositionBuilder.target(targetPosition);

      if (mapboxMarker == null)
      {
        cameraPositionBuilder.zoom(MapWrapper.DEFAULT_MAP_ZOOM_LEVEL);
      }

      final com.mapbox.mapboxsdk.camera.CameraUpdate cameraUpdate = com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newCameraPosition(cameraPositionBuilder.build());

      if (mapboxMarker == null)
      {
        final MarkerViewOptions markerViewOptions = new MarkerViewOptions();
        markerViewOptions.position(targetPosition);

        mapboxMarker = mapboxMap.addMarker(markerViewOptions);
      }
      else
      {
        mapboxMarker.setPosition(targetPosition);
      }

      mapboxMap.animateCamera(cameraUpdate);
    }
    else if (mapMode == MapWrapper.GOOGLE_MAP && googleMap != null)
    {
      final com.google.android.gms.maps.model.LatLng targetPosition = new com.google.android.gms.maps.model.LatLng(latitude, longitude);
      final com.google.android.gms.maps.model.CameraPosition.Builder cameraPositionBuilder = new com.google.android.gms.maps.model.CameraPosition.Builder(googleMap.getCameraPosition());
      cameraPositionBuilder.target(targetPosition);

      if (googleMarker == null)
      {
        cameraPositionBuilder.zoom(MapWrapper.DEFAULT_MAP_ZOOM_LEVEL);
      }

      final com.google.android.gms.maps.CameraUpdate cameraUpdate = com.google.android.gms.maps.CameraUpdateFactory.newCameraPosition(cameraPositionBuilder.build());

      if (googleMarker == null)
      {
        final MarkerOptions markerViewOptions = new MarkerOptions();
        markerViewOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.mapbox_marker_icon_default));
        markerViewOptions.position(targetPosition);

        googleMarker = googleMap.addMarker(markerViewOptions);
      }
      else
      {
        googleMarker.setPosition(targetPosition);
      }

      googleMap.animateCamera(cameraUpdate);
    }
  }

  @UiThread
  public void removeMarker()
  {
    if (mapMode == MapWrapper.MAPBOX)
    {
      if (mapboxMarker != null)
      {
        mapboxMap.removeMarker(mapboxMarker);
        mapboxMarker = null;
      }
    }
    else if (mapMode == MapWrapper.GOOGLE_MAP)
    {
      if (googleMarker != null)
      {
        googleMarker.remove();
        googleMarker = null;
      }
    }
  }

  @UiThread
  public Location getCameraPositionTarget()
  {
    final Location location = new Location("");

    if (mapMode == MapWrapper.MAPBOX)
    {
      final com.mapbox.mapboxsdk.geometry.LatLng target = mapboxMap.getCameraPosition().target;
      location.setLatitude(target.getLatitude());
      location.setLongitude(target.getLongitude());
    }
    else if (mapMode == MapWrapper.GOOGLE_MAP)
    {
      final com.google.android.gms.maps.model.LatLng target = googleMap.getCameraPosition().target;
      location.setLatitude(target.latitude);
      location.setLongitude(target.longitude);
    }

    return location;
  }
}
