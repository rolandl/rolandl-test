package fr.rolandl.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;

import com.smartnsoft.recyclerview.adapter.DiffUtilSmartRecyclerAdapter;
import com.smartnsoft.recyclerview.attributes.SmartRecyclerAttributes;
import com.smartnsoft.recyclerview.wrapper.SmartRecyclerViewWrapper;
import fr.rolandl.room.bo.Address;
import fr.rolandl.wrapper.AddressWrapper.AddressAttributes;
import fr.rolandl.wrapper.TitleWrapper.TitleAttributes;

/**
 * @author Ludovic Roland
 * @since 2017.10.21
 */
public final class AddressAdapter
    extends DiffUtilSmartRecyclerAdapter
{

  private static final class DiffUtilCallback
      extends SmartDiffUtilCallback
  {

    public DiffUtilCallback(List<? extends SmartRecyclerViewWrapper<?>> oldWrappers,
        List<? extends SmartRecyclerViewWrapper<?>> newWrappers)
    {
      super(oldWrappers, newWrappers);
    }

    @Override
    protected Object getChangePayloadCustom(int oldItemPosition, int newItemPosition, Object oldBusinessObject,
        Object newBusinessObject)
    {
      return DiffUtilCallback.ITEM_CHANGED_PAYLOAD;
    }

  }

  public AddressAdapter(Activity activity, LayoutInflater layoutInflater)
  {
    super(activity, layoutInflater);
  }

  @Override
  public boolean onBindViewHolderCustom(SmartRecyclerAttributes holder, int position, Object payload)
  {
    if (holder instanceof AddressAttributes)
    {
      ((AddressAttributes) holder).update((Address) wrappers.get(position).getBusinessObject());
      return true;
    }

    if (holder instanceof TitleAttributes)
    {
      ((TitleAttributes) holder).update();
      return true;
    }

    return false;
  }

  @Override
  public <T extends SmartDiffUtilCallback> T getDiffUtilCallback(
      List<? extends SmartRecyclerViewWrapper<?>> oldWrappers, List<? extends SmartRecyclerViewWrapper<?>> newWrappers)
  {
    return (T) new DiffUtilCallback(oldWrappers, newWrappers);
  }

}
