package fr.rolandl.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.google.android.gms.location.places.AutocompletePrediction;

/**
 * @author Ludovic Roland
 * @since 2017.10.19
 */
public final class PlacesAdapter
    extends ArrayAdapter<AutocompletePrediction>
{

  private static final class PlaceViewHolder
      implements Serializable
  {

    public TextView textView;

  }

  private final List<AutocompletePrediction> autocompletePredictions;

  public PlacesAdapter(@NonNull Context context, @NonNull List<AutocompletePrediction> autocompletePredictions)
  {
    super(context, ArrayAdapter.IGNORE_ITEM_VIEW_TYPE, autocompletePredictions);
    this.autocompletePredictions = autocompletePredictions;
  }

  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
  {
    final PlaceViewHolder viewHolder;
    final View convertViewHolder;

    if (convertView == null)
    {
      convertViewHolder = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);

      viewHolder = new PlaceViewHolder();
      viewHolder.textView = convertViewHolder.findViewById(android.R.id.text1);

      convertViewHolder.setTag(viewHolder);
    }
    else
    {
      convertViewHolder = convertView;
      viewHolder = (PlaceViewHolder) convertView.getTag();
    }

    final AutocompletePrediction autocompletePrediction = getItem(position);
    viewHolder.textView.setText(autocompletePrediction.getFullText(null));

    return convertViewHolder;
  }

  @NonNull
  @Override
  public Filter getFilter()
  {
    return new Filter()
    {

      @Override
      public CharSequence convertResultToString(Object resultValue)
      {
        return ((AutocompletePrediction) resultValue).getFullText(null);
      }

      @Override
      protected FilterResults performFiltering(CharSequence constraint)
      {
        final FilterResults filterResults = new FilterResults();
        filterResults.values = autocompletePredictions;
        filterResults.count = autocompletePredictions.size();

        return filterResults;
      }

      @Override
      protected void publishResults(CharSequence constraint, FilterResults results)
      {
        notifyDataSetChanged();
      }
    };
  }

}
