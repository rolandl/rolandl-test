package fr.rolandl;

import java.util.ArrayList;
import java.util.List;

import android.Manifest.permission;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.smartnsoft.recyclerview.wrapper.SmartRecyclerViewWrapper;
import fr.rolandl.adapter.AddressAdapter;
import fr.rolandl.fragment.MainFragment;
import fr.rolandl.fragment.PermissionsFragment;
import fr.rolandl.room.bo.Address;
import fr.rolandl.viewmodel.AddressViewModel;
import fr.rolandl.wrapper.AddressWrapper;
import fr.rolandl.wrapper.TitleWrapper;
import timber.log.Timber;

/**
 * @author Ludovic Roland
 * @since 2017.10.18
 */
public final class MainActivity
    extends AppCompatActivity
    implements Observer<List<Address>>
{

  public static final String CHECK_PERMISSIONS_AND_GPS_PROVIDER_ACTION = "checkPermissionsAndGPSProviderAction";

  public static final int REQUEST_PERMISSION_CODE = 1000;

  private RecyclerView recyclerView;

  private AddressAdapter addressAdapter;

  private DrawerLayout drawer;

  private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
  {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      if (intent.getAction().equals(MainActivity.CHECK_PERMISSIONS_AND_GPS_PROVIDER_ACTION) == true)
      {
        displayFragment();
      }
      else if (intent.getAction().equals(MainFragment.ADDRESS_SELECTED_ACTION) == true)
      {
        drawer.closeDrawer(GravityCompat.START);
      }
    }
  };

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    if (requestCode == MainActivity.REQUEST_PERMISSION_CODE)
    {
      displayFragment();
    }
    else
    {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    final Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    drawer = findViewById(R.id.drawer_layout);
    final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    toggle.syncState();

    recyclerView = findViewById(R.id.recyclerView);
    recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setHasFixedSize(true);

    addressAdapter = new AddressAdapter(this, getLayoutInflater());
    addressAdapter.setHasStableIds(true);

    recyclerView.setAdapter(addressAdapter);

    ViewModelProviders.of(this).get(AddressViewModel.class).addresses.observe(this, this);
  }

  @Override
  protected void onResume()
  {
    super.onResume();

    LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, getIntentFilter());

    displayFragment();
  }

  @Override
  protected void onPause()
  {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    super.onPause();
  }

  @Override
  public void onBackPressed()
  {
    if (drawer.isDrawerOpen(GravityCompat.START) == true)
    {
      drawer.closeDrawer(GravityCompat.START);
    }
    else
    {
      super.onBackPressed();
    }
  }

  @Override
  public void onChanged(@Nullable List<Address> addresses)
  {
    final List<SmartRecyclerViewWrapper<?>> wrappers = new ArrayList<>();
    wrappers.add(new TitleWrapper());

    for (final Address address : addresses)
    {
      wrappers.add(new AddressWrapper(address));
    }

    addressAdapter.setWrappersForDiffUtil(wrappers);
  }

  @NonNull
  private IntentFilter getIntentFilter()
  {
    final IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(MainActivity.CHECK_PERMISSIONS_AND_GPS_PROVIDER_ACTION);
    intentFilter.addAction(MainFragment.ADDRESS_SELECTED_ACTION);

    return intentFilter;
  }

  private void displayFragment()
  {
    final boolean arePermissionsDenied = ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    final boolean isGpsProviderEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    displayFragment(arePermissionsDenied == true || isGpsProviderEnabled == false ? PermissionsFragment.class : MainFragment.class);
  }

  private void displayFragment(@NonNull Class<? extends Fragment> fragmentClass)
  {
    final Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);

    if (currentFragment == null || currentFragment.getClass() != fragmentClass)
    {
      try
      {
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, fragmentClass.newInstance());
        fragmentTransaction.commitAllowingStateLoss();
      }
      catch (InstantiationException exception)
      {
        //should not happen
        Timber.w(exception, "cannot replace the fragment");
      }
      catch (IllegalAccessException exception)
      {
        //should not happen
        Timber.w(exception, "cannot replace the fragment");
      }
    }
  }

}
