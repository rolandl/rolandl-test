package fr.rolandl;

import android.app.Application;

import com.mapbox.mapboxsdk.Mapbox;
import fr.rolandl.room.db.AddressDatabase;
import timber.log.Timber;
import timber.log.Timber.DebugTree;

/**
 * @author Ludovic Roland
 * @since 2017.10.18
 */
public final class MyApplication
    extends Application
{

  @Override
  public void onCreate()
  {
    super.onCreate();

    if (BuildConfig.DEBUG == true)
    {
      Timber.plant(new DebugTree());
    }

    if (BuildConfig.APPLICATION_ID.contains("mapbox") == true)
    {
      //note: if we really really want to "protect" our access token we should use the NDK in order to load it
      //see: https://medium.com/google-developer-experts/a-follow-up-on-how-to-store-tokens-securely-in-android-e84ac5f15f17
      Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
    }

    AddressDatabase.init(this, BuildConfig.IS_TEST_MODE);
  }

}
