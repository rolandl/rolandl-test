package fr.rolandl;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.test.runner.AndroidJUnit4;

import fr.rolandl.room.bo.Address;
import fr.rolandl.room.db.AddressDatabase;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Ludovic Roland
 * @since 2017.10.21
 */
@RunWith(AndroidJUnit4.class)
public final class AddressDatabaseTest
{

  @Test
  public void testEmptyDatabse()
      throws Exception
  {
    Assert.assertEquals(AddressDatabase.getInstance().getAll().getValue(), null);
  }

  @Test
  public void addAddress()
      throws Exception
  {
    final Address address = new Address("Test", 0, 0);
    AddressDatabase.getInstance().insertAddress(address);
    final Address result = getValue(AddressDatabase.getInstance().getAll());
    Assert.assertEquals(result.label, "Test");
  }

  private Address getValue(final LiveData<List<Address>> liveData)
      throws InterruptedException
  {
    final Object[] data = new Object[1];
    final CountDownLatch countDownLatch = new CountDownLatch(1);
    final Observer<List<Address>> addressObserver = new Observer<List<Address>>()
    {
      @Override
      public void onChanged(@Nullable List<Address> address)
      {
        data[0] = address.get(0);
        countDownLatch.countDown();
        liveData.removeObserver(this);
      }
    };

    liveData.observeForever(addressObserver);
    countDownLatch.await(2, TimeUnit.SECONDS);
    return (Address) data[0];
  }

}
